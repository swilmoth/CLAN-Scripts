#!/usr/bin/env python
"""
takes a two-column file with files and offsets (ms)
and fixes chat files in a given directory.
"""

import sys
import codecs
import os
import re
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stdout)

def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('offsetfile', help = 'input')
	parser.add_argument('indir', help = 'input')
	opts = parser.parse_args()

	offsets = {}
	
	for line in codecs.open(opts.offsetfile, 'r', 'utf8'):
		line = line.strip('\n')
		f = line.split('\t')[0]
		o = line.split('\t')[1]
		offsets[f] = o
	
	timestamp = re.compile(r'(.*) .([0-9]+)_([0-9]+).$')
	for root, dirs, files in os.walk(opts.indir, topdown=False):
		for name in files:
			fullpath = os.path.join(root,name)
			if name.replace('.cha','') in offsets:
				newfile = fullpath.replace('.cha','.cha.ext')
				sys.stdout = open(newfile,'w')
				for line in codecs.open(fullpath, 'r', 'utf8'):
					line = line.strip('\n')
					if timestamp.search(line):
						start = timestamp.search(line).group(2)
						end = timestamp.search(line).group(3)
						newstart = int(start) + int(offsets[name.replace('.cha','')])
						newend = int(end) + int(offsets[name.replace('.cha','')])
						newline = line.replace(str(start),str(newstart))
						newline = newline.replace(str(end),str(newend))
						print(newline)
					else:
						print(line)

if __name__=="__main__":
	main()
