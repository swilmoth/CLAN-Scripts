#!/usr/bin/env python
"""
Compares transcription tier to mor code.
Input is a file with all .cha files grepped together, and file paths at the start of each line

***Only run this script after you have run validateMorCodes.py and countTokens.py and the
output of both scripts is 0!***
"""

import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def compareTiers(tx,mor,lexicon,mismatches):
	for i,token in enumerate(tx):
		if token in lexicon:
			code = re.sub(r'\\[PS]$','',mor[i])
			if code not in lexicon[token] and '^' not in code:
				mismatches.append(token+'\t'+code+'\t'+('^'.join(lexicon[token])))

def test(txtier,mortier,line,tx,mor):
	#testing
	if txtier:
		print 'tx'+'\t'+str(len(tx))+'\t'+line
	elif mortier:
		print 'mor'+'\t'+str(len(mor))+'\t'+line
	else:
			print '\t\t'+line

def validate(tx,mor,lineref,lexicon,mismatches):
	if len(tx) != len(mor):
		difference = len(tx)-len(mor)
		return 1
	else:
		compareTiers(tx,mor,lexicon,mismatches)
		return 0

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('-l', '--lexicon', nargs = '+', help = 'lexicon(s)') # Lexicon(s)
	parser.add_argument('-i', '--input' ,help = 'giant text file containing all .cha files')
	opts = parser.parse_args()

	lexicon = {',':['cm|cm'],'.':['.'],'!':['!'],'?':['?']}
	mismatches = []
	# import lexicon
	## Example from lexicon:
	## _ngali {[scat pro]} "_ngali&1SG/INC/O&g" =you_me=
	## Mor code:
	## pro|_ngali&1SG/INC/O&g=you_me
	for infile in opts.lexicon:
		for line in codecs.open(infile, 'r', 'utf8'):
			if not 'UTF' in line:
				line = line.strip('\n')
				splitline = line.split(' ')
				morph = splitline[0]
				pos = splitline[2][:-2]
				lemmaetc = splitline[3].strip('"')
				gloss = splitline[4].strip('=')
				morcode = pos+'|'+lemmaetc+'='+gloss
				if morph not in lexicon:
					lexicon[morph] = [morcode]
				else:
					lexicon[morph].append(morcode)

	linenumber = 0
	count = 0
	lineref = 0
	txtier = False
	mortier = False
	tx = []
	mor = []
	for line in codecs.open(opts.input, 'r', 'utf8'):
		endofutt = False
		linenumber += 1
		# cleaning up, getting rid of file path
		line = line.strip('\n')
		line = line.strip(' ')
		line = re.sub(r'^[^:]*:','',line)
		line = re.sub('  ',' ',line)
		# Which tier are we on?
		if line.startswith('%mor'):
			mortier = True
			txtier = False
		elif not line.startswith('\t'):
			mortier = False
			if tx and mor:
				count += validate(tx,mor,lineref,lexicon,mismatches)
				tx = []
				mor = []
				lineref = 0
			if line.startswith('*'):
				tx = []
				mor = []
				lineref = linenumber
				txtier = True
				mortier = False
			else:
				txtier = False

		# Transcription tier
		if txtier:
			uncertain = False
			line = re.sub('([,.!?])',r' \1',line)
			line = re.sub('  ',' ',line)
			if '\t' in line:
				secondcol = line.split('\t')[1]
				tokens = secondcol.split(' ')
				for token in tokens:
					if '[' in token:
						if not '+[' in token:
							uncertain = True
					if token:
						if not uncertain:
							if not token.startswith('&'):
								if not 'xxx' in token:
									if not u'\u0015' in token:
										if token != '+"':
											tx.append(token)
					if ']' in token:
						uncertain = False

		if mortier:
			line = re.sub('  ',' ',line)
			secondcol = line.split('\t')[1]
			codes = secondcol.split(' ')
			for code in codes:
				if code:
					mor.append(code)

	if mismatches:
		print 'Token\tMor-code\tIn lexicon'
		for mismatch in set(mismatches):
			print mismatch

if __name__=="__main__":
	main()
