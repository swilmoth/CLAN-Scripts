#!/usr/bin/env python
"""
This is similar to the FREQ command, it just lists all the tokens
on the transcription tier. The input is a giant text file with 
all sessions grepped together, with file names at the beginning of
each line.
"""

import sys
import codecs
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('input' ,help = 'giant text file containing all .cha files')
	parser.add_argument('-a', '--alltokens', action='store_true')
	opts = parser.parse_args()

	tx = []
	for line in codecs.open(opts.input, 'r', 'utf8'):
		line = line.strip('\n')
		line = line.strip(' ')
		line = re.sub(r'^[^:]*:','',line)
		line = re.sub('  ',' ',line)

		if line.startswith('*'):
			txtier = True
		elif not line.startswith('\t'):
			txtier = False

		# Transcription tier
		if txtier:
			uncertain = False
			line = re.sub('([,.!?])',r' \1',line)
			line = re.sub('  ',' ',line)
			if '\t' in line:
				secondcol = line.split('\t')[1]
				tokens = secondcol.split(' ')
				if opts.alltokens:
					for token in tokens:
						if token != 'xx' and not u'\u0015' in token and token != '+"' and not token.startswith('&='):
							tx.append(token)
				else:
					for token in tokens:
						if '[' in token:
							uncertain = True
						if not uncertain and not token.startswith('&') and token != 'xxx' and not u'\u0015' in token and token != '+"' and not bool(re.search(r'^[A-Z]',token)):
							tx.append(token)
						elif token.startswith('&='):
							tx.append(token)
						if ']' in token:
							uncertain = False

	for token in sorted(set(tx)):
		print(token)

if __name__=="__main__":
	main()
