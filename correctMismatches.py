#!/usr/bin/env python
"""
This script corrects mor-codes that don't match the lexicon, according to a
three-column mismatches file, based on the output of checkMorcodeMatches.py.

For example, the word 'kungulu' might have 'v:intran|bliding&k=bleeding'
on the mor-tier, where it should be 'v:intran|kungulu&g=bleeding'.

The 'bliding' mor-code exists in the lexicon, but isn't the right code for
this word. This script will correct the mor-codes only when they are mis-
matched in this manner.

In the mismatches input file (-m) this will look like:
kungulu	v:intran|bliding&k=bleeding	v:intran|kungulu&g=bleeding

This script is one of the final steps in cleaning up a CLAN corpus.
Before running this script, you should run:
findUnknownTokens.py and correctCHATSpelling.py
validateMorCodes.py and correctMorCodes.py
countTokens.py (until the output is 0)
checkMorcodeMatches.py --> go through the output of this file carefully!
"""

import sys
import codecs
import textwrap
from argparse import ArgumentParser,RawTextHelpFormatter
import os
import re

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def correctMor(tx,mor,mismatches):
	newmor = []
	for i,token in enumerate(tx):
		changed = False
		nSubject = False
		proSubject = False
		if mor[i].endswith('\\S'):
			nSubject = True
		if mor[i].endswith('\\P'):
			proSubject = True
		code = re.sub(r'\\[PS]$','',mor[i])

		if token in mismatches:
			for pair in mismatches[token]:
				wrongcode = pair[0]
				correctcode = pair[1]
				if code == wrongcode:
					if nSubject:
						newmor.append(correctcode+'\\S')
						changed = True
					elif proSubject:
						newmor.append(correctcode+'\\P')
						changed = True
					else:
						newmor.append(correctcode)
						changed = True
		if not changed:
			newmor.append(mor[i])
	return newmor

def main():
	parser = ArgumentParser(description = __doc__,formatter_class=RawTextHelpFormatter)
	parser.add_argument('-i', '--input' ,help = 'giant text file containing all .cha files')
	parser.add_argument('-m', '--mismatches', help = '3 column file with token, original mor code, and corrected mor code')
	opts = parser.parse_args()

	mismatches = {}
	for line in codecs.open(opts.mismatches, 'r', 'utf8'):
		line = line.strip('\n')
		token = line.split('\t')[0]
		if token not in mismatches:
			mismatches[token] = [line.split('\t')[1:]]
		else:
			mismatches[token].append(line.split('\t')[1:])

	txtier = False
	mortier = False
	tx = []
	mor = []
	newmor = []
	for origline in codecs.open(opts.input, 'r', 'utf8'):
		origline = origline.strip('\n')
		endofutt = False
		filepath = re.search(r'^(.*\.cha:)',origline).group(1)
		# cleaning up, getting rid of file path
		line = origline.strip('\n')
		line = line.strip(' ')
		line = re.sub(r'^[^:]*:','',line)
		line = re.sub('  ',' ',line)
		# Which tier are we on?
		if line.startswith('%mor'):
			mortier = True
			txtier = False
		elif not line.startswith('\t'):
			mortier = False
			if tx and mor and len(tx) == len(mor):
				# do stuff here
				newmor = correctMor(tx,mor,mismatches)
				wrapped = textwrap.wrap(' '.join(newmor),80,break_long_words=False,break_on_hyphens=False)
				print filepath+'%mor:\t'+wrapped[0]
				for morline in wrapped[1:]:
					print filepath+'\t'+morline
				tx = []
				mor = []
				newmor = []
			if line.startswith('*'):
				tx = []
				mor = []
				txtier = True
				mortier = False
			else:
				txtier = False
		if not mortier:
			print origline

		# Transcription tier
		if txtier:
			uncertain = False
			line = re.sub('([,.!?])',r' \1',line)
			line = re.sub('  ',' ',line)
			if '\t' in line:
				secondcol = line.split('\t')[1]
				tokens = secondcol.split(' ')
				for token in tokens:
					if '[' in token:
						if not '+[' in token:
							uncertain = True
					if token:
						if not uncertain:
							if not token.startswith('&'):
								if not 'xxx' in token:
									if not u'\u0015' in token:
										if token != '+"':
											tx.append(token)
					if ']' in token:
						uncertain = False

		if mortier:
			line = re.sub('  ',' ',line)
			secondcol = line.split('\t')[1]
			codes = secondcol.split(' ')
			for code in codes:
				if code:
					mor.append(code)

if __name__=="__main__":
	main()
