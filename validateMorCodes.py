#!/usr/bin/env python
"""
This script validates the mor-codes in all .cha files in a directory
(including sub-directories), and outputs any that do not match the
lexicon(s).
"""

import sys
import codecs
import re
import os
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stdout)


def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('-l', '--lexicon', nargs = '+', help = 'lexicon(s)') # Lexicon(s)
	parser.add_argument('-c', '--checkedcodes', help = 'checked mor codes') # Checked words (optional)
	parser.add_argument('-d', '--indir',nargs = '+', help = 'directory with .cha files') # Directory with .cha files

	opts = parser.parse_args()

	lexicon = []
	checkedcodes = {}
	unknowncodes = []
	extracodes = ['cm|cm','.','?','!']

	## Example from lexicon:
	## _ngali {[scat pro]} "_ngali&1SG/INC/O&g" =you_me=
	## Mor code:
	## pro|_ngali&1SG/INC/O&g=you_me

	for infile in opts.lexicon:
		for line in codecs.open(infile, 'r', 'utf8'):
			if not 'UTF' in line:
				line = line.strip('\n')
				splitline = line.split(' ')
				pos = splitline[2][:-2]
				lemmaetc = splitline[3].strip('"')
				gloss = splitline[4].strip('=')
				morcode = pos+'|'+lemmaetc+'='+gloss
				lexicon.append(morcode)

	if opts.checkedcodes:
		for line in codecs.open(opts.checkedcodes, 'r', 'utf8'):
			line = line.strip('\n')
			badCode = line.split('\t')[0]
			goodCode = line.split('\t')[1]
			checkedcodes[badCode] = goodCode

	for directory in opts.indir:
		for root, dirs, files in os.walk(directory, topdown=False):
			for name in files:
				morline = False
				fullpath = os.path.join(root,name)
				if name.endswith(".cha"):
					for line in codecs.open(fullpath, 'r', 'utf8'):
						line = line.strip('\n')
						if line.startswith('%mor'):
							morline = True
						elif not line.startswith('%mor') and not line.startswith('\t'):
							morline = False
						if morline:
							morcodes = line.split('\t')[1]
							for mor in morcodes.split(' '):
								baremor = re.sub(r'\\[PS]$','',mor)
								if baremor not in lexicon and baremor not in extracodes and not baremor.startswith('n:prop'):
									unknowncodes.append(baremor)

	checkedcodesmessage = False
	print
	if not opts.checkedcodes:
		for morcode in set(unknowncodes):
			print morcode+'\t'+morcode
	else:
		for morcode in set(unknowncodes):
			if morcode not in checkedcodes:
				print morcode+'\t'+morcode
			else:
				if not checkedcodesmessage:
					print 
					print '=== The following mor-codes are already in the \'checked codes\' file.==='
					print '===They will be corrected if you run correctMorCodes.py. ==='
					print 'Mor-code\tCorrection'	
					checkedcodesmessage = True
				print morcode+'\t'+morcode

if __name__=="__main__":
	main()
