#!/usr/bin/env python
"""
This interactive script looks up the mor-codes for a word
or string of words, according to the provided lexicon(s).

It's basically a quick version of the MOR command. Type
'exit' to quit the script.
"""

import sys
import codecs
import re
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stdout)


def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('-l', '--lexicon', nargs = '+', help = 'lexicon(s)') # Lexicon(s)
	parser.add_argument('-c', '--copy', action='store_true', help='copies the mor-codes to your clipboard for quick pasting')
	opts = parser.parse_args()

	lexicon = {',':['cm|cm'],'.':['.'],'!':['!'],'?':['?']}
	thingsthatdontshowup = ['+"','xxx']

	## Example from lexicon:
	## _ngali {[scat pro]} "_ngali&1SG/INC/O&g" =you_me=
	## Mor code:
	## pro|_ngali&1SG/INC/O&g=you_me

	for infile in opts.lexicon:
		for line in codecs.open(infile, 'r', 'utf8'):
			if not 'UTF' in line:
				line = line.strip('\n')
				splitline = line.split(' ')
				morph = splitline[0]
				pos = splitline[2][:-2]
				lemmaetc = splitline[3].strip('"')
				gloss = splitline[4].strip('=')
				morcode = pos+'|'+lemmaetc+'='+gloss
				if morph not in lexicon:
					lexicon[morph] = [morcode]
				else:
					lexicon[morph].append(morcode)
	print
	print 'Welcome to printMorCodes.py!'
	print 'You can use this script to quickly look up the mor-codes for a bunch of words instead of looking them up individually.'
	print 'Just enter your word or sentence.'
	print 'When you\'re done, type "exit".'
	print
	
	while True:
		user_input = raw_input()
		scriptoutput = []
		if user_input == 'exit':
			print 'Goodbye!'
			break

		user_input = re.sub('([,.!?])',r' \1',user_input)
		user_input = re.sub('  ',' ',user_input)
		user_input = user_input.strip(' ')
		tokenlist = user_input.split(' ')
		for token in tokenlist:
			if token in lexicon:
				code = '^'.join(lexicon[token])
				scriptoutput.append(code)
			else:
				if bool(re.search(r'^[A-Z]',token)):
					scriptoutput.append('n:prop|'+token)
				elif token not in thingsthatdontshowup and not token.startswith('&'):
					scriptoutput.append('Not-in-lexicon:'+token)

		print ' '.join(scriptoutput)+' '
		print
		if opts.copy:
			import subprocess
			process = subprocess.Popen('pbcopy', env={'LANG': 'en_AU.UTF-8'}, stdin=subprocess.PIPE)
			process.communicate((' '.join(scriptoutput)+' ').encode('utf-8'))


if __name__=="__main__":
	main()
